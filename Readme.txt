Primera Tarea: Serie de Taylor de e^x y comparacion con el valor real por medio de una grafica (Error vs N).

Autor: William Saenz Arevalo

Codigo: 01133943

Nota:
 El programa "first_hw.cpp" compilado en el archivo "first", una vez ejecutado
 pide al usuario introducir un valor numerico "n" correpondiente al grado de la
 expancion en series de Taylor de la funcion "e^x"; en seguida pide el valor de
 interes "x". Antes de imprimir los valores teoricos y aproximados, muestra 4
 columnas correspondientes a: n, n!, Taylor(n) y error(n) respectivamente.
 Finalmente en la imagen "Error_vs_N.png" se muestra el comportamiento del error
 para diferentes valores de N, para un cierto "x". 
