//expancion de taylor de e^x
#include <iostream>
#include <cmath>
#include <stdio.h>
#include <iomanip>
#include <string>

double factorial (double n)
{
  double y = 1;
  while (n>0)
    {
      y = y*n;
      --n;
    }
  return y;
}

double potencia (double x, double n)
{
  double s = 1;
  while (n>0)
    {
      s = s*x;
      --n;
    }
  return s;
}

double taylor (double x, double n)
{
  double r = 0;
  while (n>-1)
    { 
      r = r + ( ( potencia (x,n) ) / (factorial (n)));
      --n;
    }
  return r;
}

int main()
{
  double n;
  std::cout << "digite el indice superior n de la sumatoria: "; 
  std::cin >> n;
  double x;
  std::cout << "digite el valor x de interes: ";
  std::cin >> x;

  std::cout << "\n" << "los soguientes valores son: n, n!, Taylor(n), error(n), respectivamente: " << '\n'; 
  for (double i=0; i<26; i=i+1)
    {
      double error;
      error = (fabs(exp(x)-taylor(x,i)))/(exp(x));
      printf ("%.0f   %E   %.25f   %.25f   \n", i, factorial(i), taylor(x,i), error);
    }

  std::cout << "\n" << "los anteriores valores son: n, n!, Taylor(n), error(n), respectivamente." << "\n";
  printf ("El valor de e^%.1f con %.0f terminos de la serie de Taylor es: %f\n", x, n, taylor(x,n));
  printf ("El valor teorico de e^%.1f es: %.25f\n", x, exp(x));
  std::cout << "\n";

}
